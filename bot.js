const mathjs = require('mathjs')
    , http = require('http')
    , https = require('https')
    , SDK = require("balebot")
    , BaleBot = SDK.BaleBot
    , TextMessage = SDK.TextMessage
    , TOKEN = process.env.TOKEN
    , PORT = process.env.PORT || 4000
    , URL = process.env.URL

//#region for heroku error R10
http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World!\n');
}).listen(PORT, () => {
    console.log(`Server running at port: ` + PORT);
});
setInterval(() => {
    https.get(URL).on('error', (e) => {
        console.error(e)
    })
}, 300000)
//#endregion

let bot = new BaleBot(TOKEN)

let samples = `[1.2 / 3.3 + 1.7](send:1.2 / 3.3 + 1.7)
[a = 5.08 cm + 2 inch
a to inch](send:a = 5.08 cm + 2 inch
a to inch)
[a= [[1,2],[2,4]]
b = [[1,0],[0,1]]
b = a-b
b](send:a= [[1,2],[2,4]]
b = [[1,0],[0,1]]
b = a-b
b)
[9 / 3 + 2i](send:9 / 3 + 2i)
sqrt(-4)
add(2,6)
sin(90 deg)
tan(PI/3)
f(x,y) = x ^ y
f(a = 5, b = 4)
برای حذف متغیر ها از دستورات [/clear](send:/clear) و [/clr](send:/clr) استفاده کنید.`

let start = `سلام. خوش آمدید.
برای آشنایی با عبارات قابل فهم ربات دستور [/samples](send:/samples) را وارد کنید.
دستورات [/clear](send:/clear) و [/clr](send:/clr) برای حذف متغیر ها میباشند.
هر محتوای ورودی یک عبارت محاسبه شونده در نظر گرفته میشود.
به غیر از عبارات نمونه در [samples](send:/samples) شما میتوانید قابلیت های بیشتری از این ربات را کشف کنید؛
پس پیش به سوی اکتشاف 😉`

//scope created for save vars of math.js
let scope = {}
scope.values = {}
scope.clear = () => {
    scope.values = {}
}

bot.hears('start', (mes, res) => {
    //resMes is name of final var that should send to responder
    let resMes, eval
    mes = mes.text
    switch (mes) {
        case '/start': {
            resMes = new TextMessage(start)
        } break
        case '/samples': {
            resMes = new TextMessage(samples)
        } break
        case '/clear':
        case '/clr': {
            scope.clear()
            resMes = new TextMessage('متغیر ها حذف شدند.');
        } break
        default: {
            try {
                eval = mathjs.eval(mes, scope.values)
                if (eval.syntax) {
                    //if responder want set a function
                    resMes = new TextMessage(`تابع ${eval.syntax} تنظیم شد.`)
                }
                else {
                    resMes = new TextMessage(eval.toString())
                }
            }
            catch (e) {
                //wrong input or wrong var or undefined var in mes
                resMes = new TextMessage(`مقدار ورودی شما صحیح نیست، برای راهنمایی میتوانید نمونه ها را ببینید. برای اینکار [samples](send:/samples) را وارد کنید.`)
            }
        } break
    }
    res.reply(resMes)
})

/*bot.hears('/samples', (mes, res) => {
    let resMes = new TextMessage(samples)
    res.reply(resMes)
})
<! this code dont works!>*/